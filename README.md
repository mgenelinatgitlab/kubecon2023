# Click Through Demos - KubeCon 2023
## Useful for our booth demos!
Thank you to Fernando Diaz for these links [(origional google docs link).](https://docs.google.com/document/d/1CwGGAq4Zb2Hy1TMg3MFQg3g-R_MPEKisVdTLih5ycFs/edit#heading=h.h0xueu4yhjg)

## AI: GitLab Duo demos (MVC) Clickthrough demos
### [Summarize Issue Comments](https://tech-marketing.gitlab.io/static-demos/issue-summary.html)
### [Code Suggestions](https://tech-marketing.gitlab.io/static-demos/code_suggestions/guessing_game.html)
### [Explain this vulnerability](https://tech-marketing.gitlab.io/static-demos/explain-vulnerability.html)
### [Value Stream Forecasting](https://tech-marketing.gitlab.io/static-demos/value-stream-forecasting.html)
### [Generate suggested tests in merge requests](https://tech-marketing.gitlab.io/static-demos/generate-tests.html)

## More Clickthrough demos
### [GitLab Workspaces Demo](https://tech-marketing.gitlab.io/static-demos/workspaces/ws_html.html)
### [Feature Flags](https://tech-marketing.gitlab.io/static-demos/feature-flags/feature-flags-html.html)
### [CI Overview Demo](https://tech-marketing.gitlab.io/static-demos/ci_overview_v1.html)



## Matt's private project to show off connecting a k8s cluster:
[mgenelin-simplenotes-for-kubecon-2023](https://gitlab.com/gitlab-com/channel/partners/amer/mgenelin-simplenotes-for-kubecon-2023/-/clusters)
